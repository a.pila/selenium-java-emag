package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyOrders {

    @FindBy(css = "#emg-user-box #emg-user-menu .emg-fluid-text-content")
    public WebElement contulMeuWebElement;

    @FindBy(xpath = "/html//div[@id='emg-body-overlay']/div[@class='emg-container']//ul//a[@href='/history/shopping?ref=nua_order_history']")
    public WebElement comenzileMeleWebElement;

    @FindBy(css = "[name='status']")
    public WebElement drpToateComenzileWebElement;

    @FindBy(css = "#order-filters-from")
    public WebElement perioadaWebElement;
}
