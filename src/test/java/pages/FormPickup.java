package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FormPickup {

    @FindBy(css = "[title='Formular reparatie produs ']")
    public WebElement formReparatieProdusWebElement;

    @FindBy(css = "#name")
    public WebElement numePrenumeWebElement;

    @FindBy(css = "#email")
    public WebElement emailWebElement;

    @FindBy(css = "#phone")
    public WebElement telefonWebElement;

    @FindBy(css = "#bill")
    public WebElement nrFacturaWebElement;

    @FindBy(css = "#product")
    public WebElement produsWebElement;

    @FindBy(css = "#quantity")
    public WebElement cantitateWebElement;

    @FindBy(css = "#defect")
    public WebElement descriereDefectWebElement;

    @FindBy(css = "#new_address_container .gui-form-control:nth-of-type(2) [role='combobox']")
    public WebElement judetWebElement;
}
