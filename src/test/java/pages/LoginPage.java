package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

    @FindBy(xpath = "//span[contains(.,'Contul meu')]")
    public WebElement intraInContWebElement;

//    @FindBy(xpath = "//a[@href='/user/login?ref=hdr_login_btn'][contains(.,'Intra in cont')]")
//    public WebElement intraInContButtonElement;

    @FindBy(xpath = "//input[@id='user_login_email']")
    public WebElement emailWebElement;

    @FindBy(xpath = "//*[@id='user_login_continue']")
    public WebElement continuaWebElement;

    @FindBy(xpath = "//*[@id='user_login_password']'")
    //*[@id="user_login_password"]
//*[@id="user_login_password"]'
    public WebElement passwordWebElement;

    @FindBy(xpath = "//*[@id='user_login_continue']")
    public WebElement continuaLoginWebElement;

    @FindBy(css = "//img[@alt='eMAG']")
    public WebElement emagLogoWebElement;

}