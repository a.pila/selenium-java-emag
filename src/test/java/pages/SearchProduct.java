package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class SearchProduct {
    @FindBy(xpath = "//img[contains(@alt,'eMAG')]")
    public WebElement logoWebElement;

    @FindBy(xpath = "//input[contains(@type,'text')]")
    public WebElement searchBoxWebElement;

    @FindBy(xpath = "//span[contains(@class,'icon-i10-search')]")
    public WebElement searchIconWebElement;

    @FindBy(xpath = "(//a[@class='product-title js-product-url'])[1]")
    public WebElement searchResultWebElement;

    @FindBy(xpath = "//span[contains(.,'0 rezultate pentru:')]")
    public WebElement invalidSearchWebElement;

    public void assertTextPresence() {
        String actualMessageTitle = invalidSearchWebElement.getText();
        Assert.assertEquals(actualMessageTitle, "0 rezultate pentru:");
        System.out.println("Notification is verified.");
    }
}
