package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class AddToCart {

    @FindBy(xpath = "(//button[@type='submit'])[2]")
    public WebElement adaugaInCosWebElement;

    @FindBy(xpath = "//h4[contains(.,'Produsul a fost adaugat in cos')]")
    public WebElement produsAdaugatInCosWebElement;

    @FindBy(xpath = "//a[contains(.,'Vezi detalii cos')]")
    public WebElement veziDetaliiCosWebElement;

    @FindBy(xpath = "//label[contains(@for,'extraWarranty9291181265813')]")
    public WebElement chboxGarantiaPlusWebElement;

    @FindBy(xpath = "//div[@class='title'][contains(.,'Cosul tau este gol')]")
    public WebElement textCosGolWebElement;

    @FindBy(xpath = "//a[contains(.,'Sterge')]")
    public WebElement stergeProdusCosWebElement;

    @FindBy(xpath = "//a[contains(.,'Intoarce-te in magazin')]")
    public WebElement buttonBackMagazinWebElement;

    public AddToCart() {
    }

    public void assertTextProdusCos() {
        String actualMessageTitle = produsAdaugatInCosWebElement.getText();
        Assert.assertEquals(actualMessageTitle, "Produsul a fost adaugat in cos");
        System.out.println("Notification is present on page.");
    }

    public void assertTextCosGol() {
        String actualMessageTitle = textCosGolWebElement.getText();
        Assert.assertEquals(actualMessageTitle, "Cosul tau este gol");
        System.out.println("The text is present on page: 'Cosul tau este gol'.");
    }
}
