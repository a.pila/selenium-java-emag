package pages;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import ro.dataprovider.ConfigFileReader;
import ro.dataprovider.LoginFactory;

public class Base extends LoginFactory {

    LoginFactory iLoginFactory;
    LoginPage iLoginPage;

    @Test
    public void validLogin() throws InterruptedException {
        ConfigFileReader iConfigFileReader = new ConfigFileReader ();

        iLoginFactory = LoginFactory.getInstanceOfLoginFactory();
        iLoginPage = PageFactory.initElements(iLoginFactory.getDriver(), LoginPage.class);

        iLoginFactory.initialization(iConfigFileReader);

        String iUsername = iConfigFileReader.getUsername();
        iLoginFactory.setValidUsername ( iUsername, iLoginPage.emailWebElement);
        iLoginPage.continuaWebElement.click ();

        Thread.sleep (6_000);
//
//        String iPassword = iConfigFileReader.getPassword();
//        iLoginFactory.setValidPassword ( iPassword, iLoginPage.passwordWebElement);
//        iLoginPage.continuaLoginWebElement.click ();
//
//        System.out.println ( "User is successfully logged in." );
    }
}