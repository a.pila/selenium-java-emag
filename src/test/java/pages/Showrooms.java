package pages;

import com.gargoylesoftware.htmlunit.javascript.host.canvas.WebGLBuffer;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Showrooms {

    @FindBy(css = "[title='Contact']")
    public WebElement contactWebElement;

    @FindBy(css = "[title='Showroom-uri eMAG']")
    public WebElement showroomsWebElement;

    @FindBy(css = "[class='form-group col-xs-6 col-sm-12']:nth-of-type(1) .ph-label")
    public WebElement judetWebElement;

    @FindBy(css = "body [tabindex='0']:nth-child(17) [placeholder]")
    public WebElement cautaJudetWebElement;

    @FindBy(css = "[class='form-group col-xs-6 col-sm-12']:nth-of-type(2) .ph-label")
    public WebElement localitateWebElement;

    @FindBy(css = "body [tabindex='0']:nth-child(17) [placeholder]")
    public WebElement cautaLocalitateWebElement;

    @FindBy(css = "body [tabindex='0']:nth-child(18) .ph-body .ph-option:nth-of-type(1)")
    public WebElement judetAlesWebElement;

    @FindBy(css = "body [tabindex='0']:nth-child(19) .ph-body .ph-option:nth-of-type(1)")
    public WebElement sectorWebElement;

    @FindBy(css = "[data-id='a93c4458-e1be-11e8-af02-001a4a160153'] .map-point-panel-point-name")
    public WebElement showroomAlesWebElement;

    @FindBy(css = ".js-map-point-details-close")
    public WebElement inapoiListaWebElement;

    @FindBy(css = ".info-showrooms [data-phino='CollapseOne']")
    public WebElement listaShowroomsWebElement;

}
