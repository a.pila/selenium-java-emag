package utilitiesLibrary.ExtentReports;

import com.relevantcodes.extentreports.ExtentReports;

// OB: ExtentReports extent instance created here. That instance can be reachable by getReporter() method.
// In this class, we created an ExtentReports object and it can be reachable via getReporter() method.
// Also, you need to set your ExtentReports report HTML file location.

public class ExtentManager {

    private static ExtentReports extent;

    public synchronized static ExtentReports getReporter() {
        if (extent == null) {
            //Set HTML reporting file location
            String workingDir = System.getProperty("extentReportsPath");
            extent = new ExtentReports("C:\\Users\\a.pila\\Documents\\workspace-sts\\emag-project\\ExtentReports\\ExtentReportResults.html", true);
        }
        return extent;
    }
}

