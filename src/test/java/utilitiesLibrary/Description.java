package utilitiesLibrary;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Description {

    String name();

    String description();

    int priority() default 0;

    String[] groups() default {};

    boolean disabled() default false;
}
