package tests;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.MyOrders;
import pages.LoginPage;
import ro.dataprovider.ConfigFileReader;
import ro.dataprovider.LoginFactory;
import utilitiesLibrary.Description;

@Test(groups = {"AllComenzileMeleTests"})
public class MyOrdersTest {

    private LoginFactory iLoginFactory;
    private LoginPage iLoginPage;
    private MyOrders iMyOrders;

    public MyOrdersTest() {
        iLoginFactory = LoginFactory.getInstanceOfLoginFactory();
        iLoginPage = PageFactory.initElements(iLoginFactory.getDriver(), LoginPage.class);
        iMyOrders = PageFactory.initElements(iLoginFactory.getDriver(), MyOrders.class);
    }

    @BeforeTest
    public void Login() {
        ConfigFileReader configFileReader = new ConfigFileReader();

        iLoginFactory.initialization(configFileReader);

        String varUsername = configFileReader.getUsername();
        iLoginFactory.setValidUsername(varUsername, iLoginPage.emailWebElement);
        iLoginPage.continuaWebElement.click();

        String varPassword = configFileReader.getPassword();
        iLoginFactory.setValidPassword(varPassword, iLoginPage.passwordWebElement);
        iLoginPage.continuaLoginWebElement.click();
    }

    @Test(groups = {"sanity"})
    @Description(name = "Comenzile mele", description = "Navigare prin comenzile mele", priority = 1, disabled = false, groups = {"sanity"})
    public void editComenzileMele() {

        iMyOrders.contulMeuWebElement.click();
        iMyOrders.comenzileMeleWebElement.click();

        // select dropdown -> Select type
        Select drpComenzileMele = new Select(iMyOrders.drpToateComenzileWebElement);
        drpComenzileMele.selectByIndex(1);

        Select drpPerioada = new Select(iMyOrders.perioadaWebElement);
        drpPerioada.selectByValue("2017");
    }

}
