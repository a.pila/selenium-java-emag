package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.AddToCart;
import pages.Base;
import pages.LoginPage;
import pages.SearchProduct;
import ro.dataprovider.LoginFactory;
import utilitiesLibrary.Description;

@Test(groups = {"AllAddToCartTests"})
public class AddToCartTest {

    LoginFactory iLoginFactory;
    LoginPage iLoginPage;
    SearchProduct iSearchProduct;
    AddToCart iAddToCart;
    Base baseInit;

    public AddToCartTest() {
        iLoginFactory = LoginFactory.getInstanceOfLoginFactory();
        iLoginPage = PageFactory.initElements(iLoginFactory.getDriver(), LoginPage.class);
        iSearchProduct = PageFactory.initElements(iLoginFactory.getDriver(), SearchProduct.class);
        iAddToCart = PageFactory.initElements(iLoginFactory.getDriver(), AddToCart.class);
        baseInit = PageFactory.initElements(iLoginFactory.getDriver(), Base.class);
    }

    @BeforeTest
    public void validLoginTest() throws InterruptedException {
        baseInit.validLogin();
    }

    @Test(groups = {"smoke"})
    @Description(name = "Add to card", description = "Add a product to cart, go to cart, select a warranty, delete the product from cart.", priority = 1, disabled = false, groups = {"smoke"})
    public void addToCart() {

        iSearchProduct.searchBoxWebElement.sendKeys("Monitor Dell");
        System.out.println("Search by specific and valid keywords.");

        iSearchProduct.searchIconWebElement.click();
        System.out.println("Click search.");

        iSearchProduct.searchResultWebElement.click();

        iAddToCart.adaugaInCosWebElement.click();

        iAddToCart.assertTextProdusCos();

        iAddToCart.veziDetaliiCosWebElement.click();
        System.out.println("Click on: 'Vezi detalii cos'");

        iAddToCart.chboxGarantiaPlusWebElement.click();
        System.out.println("Select checkbox 'Garantia Plus 2 ANI'.");

        iAddToCart.stergeProdusCosWebElement.click();
        System.out.println("The product was deleted from cart.");

        iAddToCart.assertTextCosGol();

        iAddToCart.buttonBackMagazinWebElement.click();
        System.out.println("Click on 'Intoarce-te la magazin'.");

        iLoginFactory.getDriver().quit();
    }

//    @AfterMethod
//    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
//        if (testResult.getStatus() == ITestResult.FAILURE) {
//            System.out.println(testResult.getStatus());
//            File scrFile = ((TakesScreenshot)iLoginFactory.getDriver ()).getScreenshotAs( OutputType.FILE);
//            FileUtils.copyFile(scrFile, new File(.\src\main\resources\screenshots\" + testResult.getName() +  ".jpg");
//            //FileUtils.copyFile(scrFile, (OutputType.FILE) + testResult.getName() +  ".jpg"));
//        }
//    }

}