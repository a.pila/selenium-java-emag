package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.FormPickup;
import pages.LoginPage;
import ro.dataprovider.ConfigFileReader;
import ro.dataprovider.LoginFactory;
import utilitiesLibrary.Description;

@Test(groups = {"AllFormPickupTests"})
public class FormPickupTest {
    private LoginFactory iLoginFactory;
    private LoginPage iLoginPage;
    private FormPickup iFormPickup;

    public FormPickupTest() {
        iLoginFactory = LoginFactory.getInstanceOfLoginFactory();
        iLoginPage = PageFactory.initElements(iLoginFactory.getDriver(), LoginPage.class);
        iFormPickup = PageFactory.initElements(iLoginFactory.getDriver(), FormPickup.class);
    }

    @BeforeTest
    public void validLoginTest() {
        // create instance of ConfigFileReader (to take the properties from properties file)
        ConfigFileReader configFileReader = new ConfigFileReader();

        iLoginFactory.initialization(configFileReader);

        // create instance to take username from prop file
        String varValidUsername = configFileReader.getUsername();

        // set username instance to locator
        iLoginFactory.setValidUsername(varValidUsername, iLoginPage.emailWebElement);
        iLoginPage.continuaLoginWebElement.click();

        // create instance to take password from prop file
        String varValidPassword = configFileReader.getPassword();

        // set password instance to locator
        iLoginFactory.setValidPassword(varValidPassword, iLoginPage.passwordWebElement);
        iLoginPage.continuaLoginWebElement.click();

        System.out.println("User is logged in.");
    }

    @Test
    @Description(name = "Pickup form", description = "Complete all fields from pickup form", priority = 1, disabled = false, groups = {"regression", "sanity"})
    public void formPickup_Test() {
        iFormPickup.formReparatieProdusWebElement.click();
        iFormPickup.numePrenumeWebElement.sendKeys("Tom Smith");
        iFormPickup.emailWebElement.sendKeys("t.smith@gmail.com");
        iFormPickup.telefonWebElement.sendKeys("0726765421");
        iFormPickup.nrFacturaWebElement.sendKeys("10");
        iFormPickup.produsWebElement.sendKeys("monitor dell");
        iFormPickup.cantitateWebElement.sendKeys("5");
        iFormPickup.descriereDefectWebElement.sendKeys("lorem ipsum");
    }
}
