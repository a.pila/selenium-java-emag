package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.LoginPage;
import ro.dataprovider.ConfigFileReader;
import ro.dataprovider.LoginFactory;
import utilitiesLibrary.Description;

@Test(groups = {"AllLoginPageTests"})
public class LoginTest {

    @Test
    @Description(name = "Valid Login test", description = "This is a valid login test", priority = 1, disabled = false, groups = {"sanity"})
    public void validLoginTest() {
        ConfigFileReader configFileReader = new ConfigFileReader();
        LoginFactory iLoginFactory = LoginFactory.getInstanceOfLoginFactory();

        WebDriver iDriver = iLoginFactory.getDriver();
        LoginPage iLoginPage = PageFactory.initElements(iDriver, LoginPage.class);

        iLoginFactory.initialization(configFileReader);

        String valValidUsername = configFileReader.getUsername();
        iLoginFactory.setValidUsername(valValidUsername, iLoginPage.emailWebElement);
        iLoginPage.continuaWebElement.click();

        String valValidPassword = configFileReader.getPassword();
        iLoginFactory.setValidPassword(valValidPassword, iLoginPage.passwordWebElement);

        iLoginPage.continuaLoginWebElement.click();

        System.out.println("User is successfully logged in.");
    }
}
