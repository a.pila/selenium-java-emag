package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.SearchProduct;
import ro.dataprovider.ConfigFileReader;
import ro.dataprovider.LoginFactory;
import utilitiesLibrary.Description;

@Test(groups = {"AllSearchTests"})
public class SearchProductTest {

    private LoginFactory iLoginFactory;
    private LoginPage iLoginPage;
    private SearchProduct iSearchProduct;

    // We create an instance of each class and use it to refer its objects
    public SearchProductTest() {
        iLoginFactory = LoginFactory.getInstanceOfLoginFactory();
        iLoginPage = PageFactory.initElements(iLoginFactory.getDriver(), LoginPage.class);
        iSearchProduct = PageFactory.initElements(iLoginFactory.getDriver(), SearchProduct.class);
    }

    @BeforeTest
    public void validLoginTest() {
        ConfigFileReader iConfigFileReader = new ConfigFileReader();

        iLoginFactory.initialization(iConfigFileReader);

        String iUsername = iConfigFileReader.getUsername();
        iLoginFactory.setValidUsername(iUsername, iLoginPage.emailWebElement);
        iLoginPage.continuaWebElement.click();

        String iPassword = iConfigFileReader.getPassword();
        iLoginFactory.setValidPassword(iPassword, iLoginPage.passwordWebElement);
        iLoginPage.continuaLoginWebElement.click();

        System.out.println("User is successfully logged in.");
    }

    @Test
    @Description(name = "Valid Search", description = "This test searches for a valid product.", priority = 1, disabled = false, groups = {"regression", "windows.smoke"})
    public void validSearchTest() {

        iSearchProduct.searchBoxWebElement.sendKeys("Laptop Dell");
        System.out.println("Search by specific and valid keywords.");

        iSearchProduct.searchIconWebElement.click();
        System.out.println("Click search.");

        iSearchProduct.searchResultWebElement.click();
        System.out.println("Enter on product's page.");
        iLoginFactory.getDriver().quit();
    }

    @Test(groups = {"sanity", "linux.smoke"})
    @Description(name = "Invalid Search", description = "This test searches for an invalid product.")
    public void invalidSearchTest() {

        iSearchProduct.searchBoxWebElement.sendKeys ("qaswedrftgyhujikol");
        System.out.println ("Search by invalid keywords.");

        iSearchProduct.searchIconWebElement.click();
        System.out.println ("Click search.");

        iSearchProduct.assertTextPresence();
        iLoginFactory.getDriver().quit();
    }
}
