package tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.Showrooms;
import ro.dataprovider.ConfigFileReader;
import ro.dataprovider.LoginFactory;
import utilitiesLibrary.Description;

@Test(groups = {"AllShowroomsTests"})
public class ShowroomsTest {

    LoginFactory iLoginFactory;
    LoginPage iLoginPage;
    Showrooms iShowrooms;

    public ShowroomsTest() {
        iLoginFactory = LoginFactory.getInstanceOfLoginFactory();
        iLoginPage = PageFactory.initElements(iLoginFactory.getDriver(), LoginPage.class);
        iShowrooms = PageFactory.initElements(iLoginFactory.getDriver(), Showrooms.class);
    }

    @BeforeTest
    public void login() {

        ConfigFileReader iConfigFileReader = new ConfigFileReader();

        iLoginFactory.initialization(iConfigFileReader);

        String iUsername = iConfigFileReader.getUsername();
        iLoginFactory.setValidUsername(iUsername, iLoginPage.emailWebElement);
        iLoginPage.continuaWebElement.click();

        String iPassword = iConfigFileReader.getPassword();
        iLoginFactory.setValidPassword(iPassword, iLoginPage.passwordWebElement);
        iLoginPage.continuaLoginWebElement.click();

        System.out.println("User is successfully logged in.");
    }

    @Test
    @Description(name = "Navigate through showrooms", description = "Navigate through showrooms.", groups = {"sanity"})
    public void editShowrooms() {

        iShowrooms.contactWebElement.click();
        iShowrooms.showroomsWebElement.click();
        iShowrooms.judetWebElement.click();
        iShowrooms.cautaJudetWebElement.sendKeys("Bucuresti");
        iShowrooms.judetAlesWebElement.click();

        iShowrooms.localitateWebElement.click();
        iShowrooms.cautaLocalitateWebElement.sendKeys("Bucuresti");
        iShowrooms.sectorWebElement.click();

        iShowrooms.showroomAlesWebElement.click();
        iShowrooms.inapoiListaWebElement.click();

        iShowrooms.inapoiListaWebElement.click();
    }
}
