package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.CheckMenus;
import ro.dataprovider.LoginFactory;
import utilitiesLibrary.Description;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Test(groups = {"AllCheckMenusTests"})
public class CheckMenusTest {

    LoginFactory iLoginFactory;
    CheckMenus iCheckMenus;

    public CheckMenusTest() {
        iLoginFactory = LoginFactory.getInstanceOfLoginFactory();
        iCheckMenus = PageFactory.initElements(iLoginFactory.getDriver(), CheckMenus.class);
    }

    @Test
    @Description(name = "Check left side menus", description = "Check left side menus", priority = 1, disabled = false, groups = {"regression"})

    public void checkLeftMenus() {

        //collect all elements as WebElements
        WebElement liElements = iCheckMenus.liMenuWebElements;
        List<WebElement> actualLeftMenuList = liElements.findElements(By.tagName("li"));

        //convert the List of WebElements in a list of Strings
        List<String> actualStringList = new ArrayList<>();

        for (WebElement e : actualLeftMenuList) {
            actualStringList.add(e.getText());
        }

        //declare the expected list elements
        List<String> expectedList = new LinkedList<>();
        expectedList.add("Laptop, Tablete & Telefoane");
        expectedList.add("PC, Periferice & Software");
        expectedList.add("TV, Audio-Video & Foto");
        expectedList.add("Electrocasnice & Climatizare");
        expectedList.add("Gaming");
        expectedList.add("Fashion");
        expectedList.add("Ingrijire personala & Cosmetice");
        expectedList.add("Carti, Birotica & Cadouri");
        expectedList.add("Casa, Bricolaj & Petshop");
        expectedList.add("Sport & Activitati in aer liber");
        expectedList.add("Auto, Moto & RCA");
        expectedList.add("Jucarii, Copii & Bebe");
        expectedList.add("Supermarket");

        boolean areEqual = actualStringList.containsAll(expectedList);
        if (areEqual) {
            System.out.println("Lists are not equal.");
        } else {
            System.out.println("Lists are not equal.");
        }

        iLoginFactory.getDriver().quit();

    }

}
