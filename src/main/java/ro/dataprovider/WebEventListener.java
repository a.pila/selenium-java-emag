package ro.dataprovider;

import org.openqa.selenium.*;
import org.openqa.selenium.support.events.WebDriverEventListener;

public class WebEventListener extends BrowserDriverFactory implements WebDriverEventListener {

    @Override
    public void beforeAlertAccept(WebDriver driver) {
        System.out.println("\nbeforeAlertAccept: \n");
    }

    @Override
    public void afterAlertAccept(WebDriver driver) {
        System.out.println("\nafterAlertAccept: \n");
    }

    @Override
    public void afterAlertDismiss(WebDriver driver) {
        System.out.println("\nafterAlertDismiss: \n");
    }

    @Override
    public void beforeAlertDismiss(WebDriver driver) {
        System.out.println("\nbeforeAlertDismiss: \n");
    }

    @Override
    public void beforeNavigateTo(String url, WebDriver driver) {
        System.out.println("\nBefore navigating to: '" + url + "'\n");
    }

    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
        System.out.println("\nNavigated to:'" + url + "'\n");
    }

    @Override
    public void beforeNavigateBack(WebDriver driver) {
        System.out.println("\nbeforeNavigateBack: \n");
    }

    @Override
    public void afterNavigateBack(WebDriver driver) {
        System.out.println("\nafterNavigateBack: \n");
    }

    @Override
    public void beforeNavigateForward(WebDriver driver) {
        System.out.println("\nbeforeNavigateForward: \n");
    }

    @Override
    public void afterNavigateForward(WebDriver driver) {
        System.out.println("\nafterNavigateForward: \n");
    }

    @Override
    public void beforeNavigateRefresh(WebDriver driver) {
        System.out.println("\nbeforeNavigateRefresh: \n");
    }

    @Override
    public void afterNavigateRefresh(WebDriver driver) {
        System.out.println("\nafterNavigateRefresh: \n");
    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("\nTrying to find Element By: \n" + by.toString());
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        System.out.println("\nFound Element By: \n" + by.toString());
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        System.out.println("\nTrying to click on: \n" + element.toString());
    }

    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        System.out.println("\nClicked on: \n" + element.toString());
    }

    @Override
    public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        System.out.println("\nElement value changed to: \n" + element.toString());
    }

    @Override
    public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        System.out.println("\nElement value changed to: \n" + element.toString());
    }

    @Override
    public void beforeScript(String script, WebDriver driver) {
        System.out.println("\nbeforeScript: \n");
    }

    @Override
    public void afterScript(String script, WebDriver driver) {
        System.out.println("\nafterScript: \n");
    }

    @Override
    public void beforeSwitchToWindow(String windowName, WebDriver driver) {
        System.out.println("\nbeforeSwitchToWindow: \n");
    }

    @Override
    public void afterSwitchToWindow(String windowName, WebDriver driver) {
        System.out.println("\nafterSwitchToWindow: \n");
    }

    @Override
    public void onException(Throwable error, WebDriver driver) {
        System.out.println("\nException found: \n" + error);
    }

    @Override
    public <X> void beforeGetScreenshotAs(OutputType<X> target) {
        System.out.println("\nbeforeGetScreenshotAs: \n");
    }

    @Override
    public <X> void afterGetScreenshotAs(OutputType<X> target, X screenshot) {
        System.out.println("\nafterGetScreenshotAs: \n");
    }

    @Override
    public void beforeGetText(WebElement element, WebDriver driver) {
        System.out.println("\nGet text: \n" + element.toString());
    }

    @Override
    public void afterGetText(WebElement element, WebDriver driver, String text) {
        System.out.println("\nGot text: \n" + element.toString());
    }
}