package ro.dataprovider;

public enum BrowserType {
    CHROME,
    CHROME_HEADLESS,
    FIREFOX
}