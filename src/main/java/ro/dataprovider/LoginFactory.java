package ro.dataprovider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import java.util.concurrent.TimeUnit;

public class LoginFactory {

    private static LoginFactory inst;
    public static WebDriver driver;
    public static EventFiringWebDriver iEventFiringWebDriver;
    public static WebEventListener iWebEventListener;

    public LoginFactory() {
        driver = new BrowserDriverFactory().getDriver();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public static LoginFactory getInstanceOfLoginFactory() {
        if (inst == null) {
            inst = new LoginFactory();
        }
        return inst;
    }

    public void initListener() {
        iEventFiringWebDriver = new EventFiringWebDriver(driver);
        // Create object of EventListerHandler to register it with EventFiringWebDriver
        iWebEventListener = new WebEventListener();
        iEventFiringWebDriver.register(iWebEventListener);
        driver = iEventFiringWebDriver;
    }

    public void initialization(ConfigFileReader configFileReader) {
        inst.initListener();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitlyWait(), TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitlyWait(), TimeUnit.SECONDS);
        driver.navigate().to(configFileReader.getApplicationUrl());
    }

    public void setValidUsername(String username, WebElement emailElement) {
        emailElement.sendKeys(username);
    }

    public void setValidPassword(String password, WebElement passwordElement) {
        passwordElement.sendKeys(password);
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    public void setInvalidUsername(String username, WebElement emailElement) {
        emailElement.sendKeys(username);
    }

    public void setInvalidPassword(String password, WebElement passwordElement) {
        passwordElement.sendKeys(password);
    }

    public void checkLogo(By logoBy) {
        WebElement image = driver.findElement(logoBy);

        if (image.isDisplayed() == true) {
            System.out.println("Logo image is present on page.");
        } else {
            System.out.println("Logo image is not present on page.");
        }
    }

    public void clickLogOut(By logOutBy) {
        WebElement logOut = driver.findElement(logOutBy);
        logOut.click();
    }


}


