package ro.dataprovider;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BrowserDriverFactory {

    private final ConfigFileReader iConfigFileReader;
    private FirefoxOptions iFirefoxOptions = new FirefoxOptions();
    private ChromeOptions iChromeOptions = new ChromeOptions();
    private ChromeOptions iChromeHeadOptions = new ChromeOptions();

    public BrowserDriverFactory() {
        iConfigFileReader = new ConfigFileReader();
    }

    public RemoteWebDriver getDriver() {
        String configFileBrowserValue = iConfigFileReader.getPropertyValue("browser");
        BrowserType browser = BrowserType.valueOf(configFileBrowserValue);

        switch (browser) {
            case CHROME:
                // WebDriverManager.chromedriver().setup();
                return new ChromeDriver(iChromeOptions);
//            case CHROME_HEADLESS:
//                return  new ChromeDriver (iChromeHeadOptions);
            case FIREFOX:
                // WebDriverManager.firefoxdriver().setup();
                return new FirefoxDriver(iFirefoxOptions);
        }
        throw new RuntimeException("Provided browser value from config file is not valid for the current implementation.");
    }

    private void setChromePreferences() {
        iChromeOptions.setBinary(iConfigFileReader.getChromeBinaryPath());
        iChromeOptions.addArguments("--incognito");
    }

//    private void setChromeHeadPreferences(ChromeOptions chromeHeadPreferences) {
//        iChromeHeadOptions.addArguments ( "--headless" );
//        iChromeHeadOptions.addArguments ( "window-size=1920x1080" );
//        iChromeHeadOptions.addArguments ( "--start-maximized" );
//        iChromeHeadOptions.addArguments ( "--disable-popup-blocking" );
//        iChromeHeadOptions.addArguments("disable-infobars");
//        iChromeHeadOptions.addArguments("--incognito");
//        iChromeHeadOptions.addArguments("--no-default-browser-check");
//        iChromeHeadOptions.setBinary(iConfigFileReader.getChromeHeadlessBinaryPath());
//    }

    private void setFirefoxPreferences(FirefoxOptions iFirefoxOptions) {
        iFirefoxOptions.setBinary(iConfigFileReader.getFirefoxBinaryPath());
        iFirefoxOptions.setLogLevel(FirefoxDriverLogLevel.DEBUG);

        final FirefoxProfile fp = new FirefoxProfile();
        fp.setAcceptUntrustedCertificates(true);
        fp.setPreference("security.enable_java", true);
        fp.setPreference("plugin.state.java", 2);
        fp.setPreference("app.update.auto", false);
        fp.setPreference("app.update.enabled", false);
        fp.setPreference("app.update.silent", false);
    }

    private void setEnvironmentVariables(BrowserType browser) {
        String driverPath = iConfigFileReader.getDriverPath();

        switch (browser) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", driverPath);
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", driverPath);
//            case CHROME_HEADLESS:
//                System.setProperty("webdriver.chrome.driver", driverPath);
            default:
                throw new IllegalArgumentException("Unknown browser: " + browser);
        }
    }
}
