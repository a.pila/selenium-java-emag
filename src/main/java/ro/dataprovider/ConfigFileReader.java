package ro.dataprovider;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigFileReader {

    private static final String propertyFilePath = ".\\src\\main\\resources\\configuration.properties";

    private Properties properties = new Properties();

    public ConfigFileReader() {
        InputStream iConfigFileReader = getClass().getClassLoader().getResourceAsStream("configuration.properties");

        try {
            properties.load(iConfigFileReader);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (iConfigFileReader != null) {
                try {
                    iConfigFileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getDriverPath() {
        String driverPath;
        driverPath = properties.getProperty("driverPath");
        if (driverPath != null) return driverPath;
        else throw new RuntimeException("driverPath is not specified in the Configuration.properties file.");
    }

    public String getChromeBinaryPath() {
        String chromeBinaryPath;
        chromeBinaryPath = properties.getProperty("chromeBinaryPath");
        if (chromeBinaryPath != null) return chromeBinaryPath;
        else throw new RuntimeException("chromeBinaryPath is not specified in the Configuration.properties file.");
    }

    public String getChromeHeadlessBinaryPath() {
        String chromeBinaryPath;
        chromeBinaryPath = properties.getProperty("chromeHeadlessBinaryPath");
        if (chromeBinaryPath != null) return chromeBinaryPath;
        else
            throw new RuntimeException("chromeHeadlessBinaryPath is not specified in the Configuration.properties file.");
    }

    public String getFirefoxBinaryPath() {
        String firefoxBinaryPath;
        firefoxBinaryPath = properties.getProperty("firefoxBinaryPath");
        if (firefoxBinaryPath != null) return firefoxBinaryPath;
        else throw new RuntimeException("firefoxBinaryPath is not specified in the Configuration.properties file.");
    }

    public String getExtentReportsPath() {
        String extentReportsPath = properties.getProperty("extentReportsPath");
        if (extentReportsPath != null) return extentReportsPath;
        else throw new RuntimeException("extentReportsPath is not specified in the Configuration.properties file.");
    }

    public long getImplicitlyWait() {
        String implicitlyWait = properties.getProperty("implicitlyWait");
        if (implicitlyWait != null) return Long.parseLong(implicitlyWait);
        else throw new RuntimeException("implicitlyWait is not specified in the Configuration.properties file.");
    }

    public String getApplicationUrl() {
        String url = properties.getProperty("url");
        if (url != null) return url;
        else throw new RuntimeException("url is not specified in the Configuration.properties file.");
    }

    public String getUsername() {
        String validUsername = properties.getProperty("validUsername");
        if (validUsername != null) return validUsername;
        else throw new RuntimeException("Valid username is not specified in the Configuration.properties file.");
    }

    public String getPassword() {
        String validPassword = properties.getProperty("validPassword");
        if (validPassword != null) return validPassword;
        else throw new RuntimeException("Valid password is not specified in the Configuration.properties file.");
    }

    public String getInvalidUsername() {
        String invalidUsername = properties.getProperty("invalidUsername");
        if (invalidUsername != null) return invalidUsername;
        else throw new RuntimeException("Invalid username is not specified in the Configuration.properties file.");
    }

    public String getInvalidPassword() {
        String invalidPassword = properties.getProperty("invalidPassword");
        if (invalidPassword != null) return invalidPassword;
        else throw new RuntimeException("Invalid password is not specified in the Configuration.properties file.");
    }

    public String getPropertyValue(String browser) {
        return properties.getProperty("browser");
    }
}